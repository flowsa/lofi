<?php
/**
 * lofi plugin for Craft CMS 3.x
 *
 * Provide lofi version of an image 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\lofi;

use flowsa\lofi\services\LofiService as LofiServiceService;
use flowsa\lofi\twigextensions\LofiTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;

use yii\base\Event;

/**
 * Class Lofi
 *
 * @author    Richard Frank
 * @package   Lofi
 * @since     0.0.1
 *
 * @property  LofiServiceService $lofiService
 */
class Lofi extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Lofi
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '0.0.1';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Craft::$app->view->registerTwigExtension(new LofiTwigExtension());

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'lofi',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
