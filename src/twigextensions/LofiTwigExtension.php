<?php
/**
 * lofi plugin for Craft CMS 3.x
 *
 * Provide lofi version of an image 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\lofi\twigextensions;

use flowsa\lofi\Lofi;

use Craft;

/**
 * @author    Richard Frank
 * @package   Lofi
 * @since     0.0.1
 */
class LofiTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Lofi';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('lofi', [$this, 'lofi']),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('someFunction', [$this, 'someInternalFunction']),
        ];
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function lofi($asset)
    {

      $volumePath = $asset->getVolume()->settings['path'];
      $folderPath = $asset->getFolder()->path;
      $assetFilePath = \Yii::getAlias($volumePath) . "/". $folderPath . $asset->filename;
      
      $base64 = Lofi::$plugin->lofiService->getLofiVersionOfImage($assetFilePath);


      return $base64;
        // return $result;
    }
}
