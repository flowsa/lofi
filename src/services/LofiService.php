<?php
/**
 * lofi plugin for Craft CMS 3.x
 *
 * Provide lofi version of an image 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */
namespace flowsa\lofi\services;

use flowsa\lofi\Lofi;

use Craft;
use craft\base\Component;
use flowsa\lofi\lib\potracio\Potracio;



/**
 * @author    Richard Frank
 * @package   Lofi
 * @since     0.0.1
 */
class LofiService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function getLofiVersionOfImage($imagePath)
    {

      $pot = new Potracio();
      $pot->loadImageFromFile($imagePath);

      $parameters = [
        'turnpolicy'   => "white",
        'turdsize'     => 10000,
        'alphamax'     => 100,
        'opttolerance' => 1000,
      ];

      $pot->setParameter($parameters);
      $pot->process();

      $svg = $pot->getSVG(1, "none");

      $base64 = base64_encode($svg);
      // $imageTag = "<img alt=\"\" src=\"data:image/svg+xml;base64,$base64 \">";

      $imageSrc = "data:image/svg+xml;base64,$base64";

      return $imageSrc;
      // echo $imageTag;

    }
}
