<?php
/**
 * lofi plugin for Craft CMS 3.x
 *
 * Provide lofi version of an image 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

/**
 * @author    Richard Frank
 * @package   Lofi
 * @since     0.0.1
 */
return [
    'lofi plugin loaded' => 'lofi plugin loaded',
];
